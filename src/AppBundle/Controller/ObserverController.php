<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 19/06/2018
 * Time: 13:08
 */

namespace AppBundle\Controller;


use AppBundle\Patterns\Observer\LogListener;
use AppBundle\Patterns\Observer\StatusEvent;
use AppBundle\Patterns\Observer\StatusListener;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ObserverController
 *
 * @package AppBundle\Controller
 */
class ObserverController extends Controller
{
    /**
     * @Route("/observer", name="observer")
     */
    public function indexAction()
    {
        $statusListener = new StatusListener();
        $logListener    = new LogListener();

        $event = (new StatusEvent())
            ->setStatus('START')
            ->setTitle('Proccess has been')
            ->attach($statusListener)
            ->attach($logListener);

        $event->notify();

        $event->detach($logListener)->notify();

        exit;
    }
}