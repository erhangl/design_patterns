<?php
/**
 * Oluşturulma tarihi : 24/04/2017
 */

namespace AppBundle\Controller;

use AppBundle\Patterns\Bridge\User;
use AppBundle\Patterns\Command\CommandRegistry;
use AppBundle\Patterns\Command\EmailCommand;
use AppBundle\Patterns\Command\PushNotificationCommand;
use AppBundle\Patterns\Command\SmsCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class CommandController
 *
 * @package AppBundle\Controller
 */
class CommandController extends Controller
{
    /**
     * @Route("/command", name="command")
     */
    public function indexAction()
    {
        $user = new User();
        $user->setName('Erhan')
             ->setSurname('GÜL')
             ->setEmail('erhangl@gmail.com')
             ->setGsm('05336676853');


        $emailCommand            = new EmailCommand($user);
        $smsCommand              = new SmsCommand($user);
        $pushNotificationCommand = new PushNotificationCommand($user);

        $commandRegistry = new CommandRegistry();
        $commandRegistry->addCommand($emailCommand)
            ->addCommand($smsCommand)
            ->addCommand($pushNotificationCommand);

        $commandRegistry->execute();

        exit;
    }
}
