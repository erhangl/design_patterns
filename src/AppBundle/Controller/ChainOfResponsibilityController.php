<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 14/06/2018
 * Time: 18:17
 */

namespace AppBundle\Controller;


use AppBundle\Patterns\ChainOfResponsibility\StepFourHandler;
use AppBundle\Patterns\ChainOfResponsibility\StepOneHandler;
use AppBundle\Patterns\ChainOfResponsibility\StepThreeHandler;
use AppBundle\Patterns\ChainOfResponsibility\StepTwoHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ChainOfResponsibilityController
 *
 * @package AppBundle\Controller
 */
class ChainOfResponsibilityController extends Controller
{
    /**
     * @Route("chain-of-responsibility")
     */
    public function indexAction()
    {
        $stepOne   = new StepOneHandler();
        $stepTwo   = new StepTwoHandler();
        $stepThree = (new StepThreeHandler())->setIsNext(true);
        $stepFour  = new StepFourHandler();

        $stepThree->setHandler($stepFour);
        $stepTwo->setHandler($stepThree);
        $stepOne->setHandler($stepTwo);

        $stepOne->handle();

        exit;
    }
}