<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Controller;

use AppBundle\Patterns\DataMapper\CarMapper;
use AppBundle\Patterns\DataMapper\StorageAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MapperController
 *
 * @package AppBundle\Controller
 */
class MapperController extends Controller
{
    /**
     * @Route("/mapper", name="mapper")
     */
    public function indexAction()
    {
        $data = [
            [
                'brand'     => 'Ford',
                'model'     => 'Focus',
                'modelYear' => 2010,
                'color'     => 'Beyaz'
            ],
            [
                'brand'     => 'Renault',
                'model'     => 'Megane',
                'modelYear' => 2010,
                'fuel'      => 'Dizel'
            ],
            [
                'brand'     => 'BMW',
                'model'     => '3.16',
                'modelYear' => 2014,
                'fuel'      => 'Dizel'
            ]
        ];

        $storage = new StorageAdapter($data);
        $mapper  = new CarMapper($storage);

        var_dump($mapper->findBy(['modelYear' => 2014]));

        exit;
    }
}
