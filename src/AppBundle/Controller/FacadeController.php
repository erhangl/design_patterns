<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Controller;

use AppBundle\Patterns\Facade\CarFacade;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class FacadeController
 *
 * @package AppBundle\Controller
 */
class FacadeController extends Controller
{
    /**
     * @Route("/facade", name="facade")
     */
    public function indexAction()
    {
        $car = new CarFacade();
        echo $car->getBrand()->getBrand() . '<br>';
        echo $car->getBrand()->getModel() . '<br>';

        echo $car->getEngine()->getEnginePower() . '<br>';
        echo $car->getEngine()->getEngineCapacity() . '<br>';

        echo $car->getGear()->getGearCategory() . ' ';
        echo $car->getGear()->getGearType() . '<br>';

        exit;
    }
}
