<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 04/07/2018
 * Time: 14:28
 */

namespace AppBundle\Controller;


use AppBundle\Patterns\Memento\Product;
use AppBundle\Patterns\Memento\ProductMemento;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MementoController
 *
 * @package AppBundle\Controller
 */
class MementoController extends Controller
{
    /**
     * @Route("/memento", name="memento")
     */
    public function indexAction()
    {
        $product = (new Product())
            ->setAmount(100)
            ->setStock(5)
            ->setStatus(true);

        $memento = new ProductMemento($product);

        echo $product->getStock() . "<br>";

        $product->setStock(4);

        echo $product->getStock() . "<br>";

        $product->setStock(3);

        echo $product->getStock() . "<br>";

        $memento->getStock($product);

        echo $product->getStock() . "<br>";

        exit;
    }
}