<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Controller;

use AppBundle\Patterns\Bridge\BlockMessageNotification;
use AppBundle\Patterns\Bridge\MailSender;
use AppBundle\Patterns\Bridge\RegisterMessageNotification;
use AppBundle\Patterns\Bridge\SmsSender;
use AppBundle\Patterns\Bridge\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class BridgeController
 *
 * @package AppBundle\Controller
 */
class BridgeController extends Controller
{
    /**
     * @Route("/bridge", name="bridge")
     */
    public function indexAction()
    {
        $user = new User();
        $user->setName('Erhan')
             ->setSurname('GÜL')
             ->setEmail('erhangl@gmail.com')
             ->setGsm('05336676853');

        $smsSender  = new SmsSender($user);
        $mailSender = new MailSender($user);

        $register = new RegisterMessageNotification();
        $register->setSender($smsSender)->send();
        $register->setSender($mailSender)->send();

        $block = new BlockMessageNotification();
        $block->setSender($smsSender)->send();
        $block->setSender($mailSender)->send();

        exit;
    }
}
