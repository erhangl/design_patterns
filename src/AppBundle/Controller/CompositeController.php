<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Controller;

use AppBundle\Patterns\Composite\Child;
use AppBundle\Patterns\Composite\Composite;
use AppBundle\Patterns\Composite\Product;
use AppBundle\Patterns\Composite\Property;
use AppBundle\Patterns\Composite\PropertyComposite;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class CompositeController
 *
 * @package AppBundle\Controller
 */
class CompositeController extends Controller
{
    /**
     * @Route("/composite", name="composite")
     */
    public function indexAction()
    {
        $composite = (new PropertyComposite());


        $property1 = (new Property())
            ->setName('1')
            ->setValue('1');

        $property2 = (new Property())
            ->setName('2')
            ->setValue('2');

        $composite->add($property1);
        $composite->add($property2);

        $product = (new Product())
            ->setName('pro1')
            ->setProductCode('sdfsdf')
            ->setProperty($composite);

        echo $product->getProperty()->getName();

        exit;
        $child1 = new Child('Child1');
        $child2 = new Child('Child2');

        $composite = new Composite('Composite1');
        $composite->add($child1);
        $composite->add($child2);

        $child3 = new Child('Child3');
        $child4 = new Child('Child4');

        $composite2 = new Composite('Composite2');
        $composite2->add($child3);
        $composite2->add($child4);

        $composite->add($composite2);

        $composite3 = new Composite('Composite3');
        $composite4 = new Composite('Composite4');

        $child5 = new Child('Child5');

        $composite4->add($child5);
        $composite3->add($composite4);
        $composite->add($composite3);

        $composite2->run();

        exit;
    }
}
