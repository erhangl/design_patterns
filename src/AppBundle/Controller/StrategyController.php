<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 05/07/2018
 * Time: 16:46
 */

namespace AppBundle\Controller;


use AppBundle\Patterns\Strategy\CC;
use AppBundle\Patterns\Strategy\PosPay;
use AppBundle\Patterns\Strategy\Wallet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StrategyController
 *
 * @package AppBundle\Controller
 */
class StrategyController
{
    /**
     * @Route("/strategy", name="strategy")
     */
    public function indexAction(Request $request)
    {
        $pos = (new PosPay())->setMethod('cc');

        $pos1 = new Wallet();
        $pos2 = new CC();

        $pos->add($pos1);
        $pos->add($pos2);

        $pos->pay();

        exit;
    }
}