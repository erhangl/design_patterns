<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 19/06/2018
 * Time: 11:55
 */

namespace AppBundle\Patterns\Observer;


/**
 * Class AbstractEvent
 *
 * @package AppBundle\Patterns\Observer
 */
abstract class AbstractEvent
{
    private $title;

    protected $listeners = [];

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return AbstractEvent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param AbstractListener $listener
     *
     * @return AbstractEvent
     */
    public function attach(AbstractListener $listener)
    {
        $this->listeners[] = $listener;

        return $this;
    }

    /**
     * @param AbstractListener $listener
     *
     * @return AbstractEvent
     */
    public function detach(AbstractListener $listener)
    {
        foreach ($this->listeners as $listenerKey => $listenerIn) {
            if ($listener == $listenerIn) {
                unset($this->listeners[$listenerKey]);
            }
        }

        return $this;
    }

    abstract public function notify();
}