<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 19/06/2018
 * Time: 13:16
 */

namespace AppBundle\Patterns\Observer;


/**
 * Class LogListener
 *
 * @package AppBundle\Patterns\Observer
 */
class LogListener extends AbstractListener
{
    /**
     * @param AbstractEvent $event
     */
    public function update(AbstractEvent $event)
    {
        echo '<br>';
        echo $event->getTitle() . ' logged';
    }
}
