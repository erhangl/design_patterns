<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 19/06/2018
 * Time: 13:02
 */

namespace AppBundle\Patterns\Observer;


/**
 * Class StatusEvent
 *
 * @package AppBundle\Patterns\Observer
 */
class StatusEvent extends AbstractEvent
{
    private $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return StatusEvent
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function notify()
    {
        /** @var AbstractListener $listener */
        foreach ($this->listeners as $listener) {
            $listener->update($this);
        }
    }
}
