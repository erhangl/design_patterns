<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 19/06/2018
 * Time: 11:49
 */

namespace AppBundle\Patterns\Observer;


/**
 * Class Observer
 *
 * @package AppBundle\Patterns\Observer
 */
abstract class AbstractListener
{
    /**
     * @param AbstractEvent $event
     */
    abstract public function update(AbstractEvent $event);
}