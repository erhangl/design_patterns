<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 19/06/2018
 * Time: 13:05
 */

namespace AppBundle\Patterns\Observer;


/**
 * Class StatusListener
 *
 * @package AppBundle\Patterns\Observer
 */
class StatusListener extends AbstractListener
{
    /**
     * @param AbstractEvent $event
     */
    public function update(AbstractEvent $event)
    {
        echo '<br>';
        echo $event->getTitle() . ' ' . $event->getStatus();
    }
}