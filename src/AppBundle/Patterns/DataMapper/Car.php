<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\DataMapper;

/**
 * Class Car
 * 
 * @package AppBundle\Patterns\DataMapper
 */
class Car
{
    private $brand;

    private $model;

    private $modelYear;

    private $color;

    private $fuel;

    private $gear;

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     *
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     *
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModelYear()
    {
        return $this->modelYear;
    }

    /**
     * @param mixed $modelYear
     *
     * @return $this
     */
    public function setModelYear($modelYear)
    {
        $this->modelYear = $modelYear;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     *
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param mixed $fuel
     *
     * @return $this
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGear()
    {
        return $this->gear;
    }

    /**
     * @param mixed $gear
     *
     * @return $this
     */
    public function setGear($gear)
    {
        $this->gear = $gear;

        return $this;
    }
}
