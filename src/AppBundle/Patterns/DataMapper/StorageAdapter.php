<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\DataMapper;

/**
 * Class StorageAdapter
 *
 * @package AppBundle\Patterns\DataMapper
 */
class StorageAdapter
{
    /** @var array $data */
    private $data = [];

    /**
     * StorageAdapter constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param array $conditions
     *
     * @return \Generator
     */
    public function findBy(array $conditions)
    {
        foreach ($this->data as $dataKey => $dataValue) {
            $comparison = true;

            foreach ($conditions as $key => $val) {
                if (!isset($dataValue[$key]) || $dataValue[$key] != $val) {
                    $comparison = false;
                }
            }

            if ($comparison) {
                yield  $this->data[$dataKey];
            }
        }
    }
}
