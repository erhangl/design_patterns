<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\DataMapper;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CarMapper
 *
 * @package AppBundle\Patterns\DataMapper
 */
class CarMapper
{
    /** @var StorageAdapter $adapter */
    private $adapter;

    /**
     * CarMapper constructor.
     *
     * @param StorageAdapter $adapter
     */
    public function __construct(StorageAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function findBy(array $conditions)
    {
        $result = $this->adapter->findBy($conditions);

        if (!$result->valid()) {
            return null;
        }

        return $this->mapData($result);
    }

    /**
     * @param \Iterator $data
     *
     * @return ArrayCollection
     */
    public function mapData(\Iterator $data)
    {
        $cars = new ArrayCollection();

        foreach ($data as $carData) {
            $car = new Car();
            foreach ($carData as $key => $val) {
                if (method_exists($car, 'set' . ucfirst($key))) {
                    $car->{'set' . ucfirst($key)}($val);
                }
            }
            $cars->add($car);
        }

        return $cars;
    }
}