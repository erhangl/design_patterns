<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 14/06/2018
 * Time: 18:12
 */

namespace AppBundle\Patterns\ChainOfResponsibility;


/**
 * Class StepThreeHandler
 *
 * @package AppBundle\ChainOfResponsibility
 */
class StepThreeHandler extends AbstractHandler
{
    /** @var boolean */
    private $isNext;

    /**
     * @return bool
     */
    public function isNext()
    {
        return $this->isNext;
    }

    /**
     * @param bool $isNext
     *
     * Buradaki parametre istersek bir handler ın içerisindeki bir durumu o anda kontrol edip
     * bir diğer handler a geçip geçmeme kararını verebileceğimiz için örnek olarak var.
     * Normalde dışarıdan bu şekilde set edilecek bir parametre değilde handle içerisinde bir if ya da try catch
     * bloğuna bakarak process i orada durdurabiliriz.
     *
     * @return StepThreeHandler
     */
    public function setIsNext($isNext)
    {
        $this->isNext = $isNext;

        return $this;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        echo 'Process Step Three <br>';

        if ($this->getNextHandler() && $this->isNext()) {
            $this->getNextHandler()->handle();

            return true;
        }

        echo 'Process Step Four Cannot Run!';
    }
}