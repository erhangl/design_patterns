<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 14/06/2018
 * Time: 18:03
 */

namespace AppBundle\Patterns\ChainOfResponsibility;


/**
 * Interface HandlerInterface
 *
 * @package AppBundle\ChainOfResponsibility
 */
interface HandlerInterface
{
    /**
     * @param HandlerInterface $handler
     *
     * @return mixed
     */
    public function setHandler(HandlerInterface $handler);

    /**
     * @return mixed
     */
    public function handle();
}