<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 14/06/2018
 * Time: 18:07
 */

namespace AppBundle\Patterns\ChainOfResponsibility;


/**
 * Class AbstractHandler
 *
 * @package AppBundle\ChainOfResponsibility
 */
abstract class AbstractHandler implements HandlerInterface
{
    /** @var HandlerInterface */
    private $nextHandler;

    /**
     * @return HandlerInterface
     */
    public function getNextHandler()
    {
        return $this->nextHandler;
    }
    /**
     * @param HandlerInterface $handler
     *
     * @return mixed
     */
    public function setHandler(HandlerInterface $handler)
    {
        $this->nextHandler = $handler;

        return $this;
    }

    /**
     * @return mixed
     */
    abstract public function handle();
}