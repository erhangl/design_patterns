<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 14/06/2018
 * Time: 18:16
 */

namespace AppBundle\Patterns\ChainOfResponsibility;


/**
 * Class StepFourHandler
 *
 * @package AppBundle\ChainOfResponsibility
 */
class StepFourHandler extends AbstractHandler
{
    /**
     * @return mixed
     */
    public function handle()
    {
        echo 'Process Step Four <br>';
    }
}