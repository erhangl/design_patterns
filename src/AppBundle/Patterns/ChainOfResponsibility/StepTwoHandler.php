<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 14/06/2018
 * Time: 18:11
 */

namespace AppBundle\Patterns\ChainOfResponsibility;


/**
 * Class StepTwoHandler
 *
 * @package AppBundle\ChainOfResponsibility
 */
class StepTwoHandler extends AbstractHandler
{
    /**
     * @return mixed
     */
    public function handle()
    {
        echo 'Process Step Two <br>';

        if ($this->getNextHandler()) {
            $this->getNextHandler()->handle();
        }
    }
}