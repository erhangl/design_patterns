<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 14/06/2018
 * Time: 18:05
 */

namespace AppBundle\Patterns\ChainOfResponsibility;


/**
 * Class StepOneHandler
 *
 * @package AppBundle\ChainOfResponsibility
 */
class StepOneHandler extends AbstractHandler
{
    /**
     * @return mixed
     */
    public function handle()
    {
        echo 'Process Step One <br>';

        if ($this->getNextHandler()) {
            $this->getNextHandler()->handle();
        }
    }
}