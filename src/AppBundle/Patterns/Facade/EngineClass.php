<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Facade;

/**
 * Class EngineClass
 *
 * @package AppBundle\Patterns\Facade
 */
class EngineClass
{
    private $enginePower = '101 - 125 HP';

    private $engineCapacity = '1601 - 1800 cm3';

    private $fuel = 'Dizel';

    /**
     * @return string
     */
    public function getEnginePower()
    {
        return $this->enginePower;
    }

    /**
     * @return string
     */
    public function getEngineCapacity()
    {
        return $this->engineCapacity;
    }

    /**
     * @return string
     */
    public function getFuel()
    {
        return $this->fuel;
    }
}
