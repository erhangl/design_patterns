<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Facade;

/**
 * Class CarFacade
 *
 * @package AppBundle\Patterns\Facade
 */
class CarFacade
{
    /** @var EngineClass $engine */
    private $engine;
    /** @var BrandClass $brand */
    private $brand;
    /** @var GearClass $gear */
    private $gear;

    /**
     * CarFacade constructor.
     */
    public function __construct()
    {
        $this->engine = new EngineClass();
        $this->brand  = new BrandClass();
        $this->gear   = new GearClass();
    }

    /**
     * @return mixed
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @return mixed
     */
    public function getGear()
    {
        return $this->gear;
    }
}
