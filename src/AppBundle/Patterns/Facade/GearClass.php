<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Facade;

/**
 * Class GearClass
 *
 * @package AppBundle\Patterns\Facade
 */
class GearClass
{
    private $gearType = 'Otomatik';

    private $gearCategory = 'DSG';

    /**
     * @return mixed
     */
    public function getGearType()
    {
        return $this->gearType;
    }

    /**
     * @return string
     */
    public function getGearCategory()
    {
        return $this->gearCategory;
    }
}
