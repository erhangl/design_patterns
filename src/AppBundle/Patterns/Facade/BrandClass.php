<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Facade;

/**
 * Class BrandClass
 *
 * @package AppBundle\Patterns\Facade
 */
class BrandClass
{
    private $brand = 'Ford';

    private $model = 'Mondeo';

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }
}