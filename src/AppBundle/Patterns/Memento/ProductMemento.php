<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 04/07/2018
 * Time: 14:21
 */

namespace AppBundle\Patterns\Memento;


/**
 * Class ProductMemento
 *
 * @package AppBundle\Patterns\Memento
 */
class ProductMemento
{
    private $amount;

    private $stock;

    private $status;

    /**
     * ProductMemento constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->amount = $product->getAmount();
        $this->stock  = $product->getStock();
        $this->status = $product->getStatus();
    }

    /**
     * @param Product $product
     *
     * @return Product
     */
    public function getAmount(Product $product)
    {
        return $product->setAmount($this->amount);
    }

    /**
     * @param Product $product
     */
    public function setAmount(Product $product)
    {
        $this->amount = $product->getAmount();
    }

    /**
     * @param Product $product
     *
     * @return Product
     */
    public function getStock(Product $product)
    {
        return $product->setStock($this->stock);
    }

    /**
     * @param Product $product
     */
    public function setStock(Product $product)
    {
        $this->stock = $product->getStock();
    }

    /**
     * @param Product $product
     *
     * @return Product
     */
    public function getStatus(Product $product)
    {
        return $product->setStatus($this->status);
    }

    /**
     * @param Product $product
     */
    public function setStatus(Product $product)
    {
        $this->status = $product->getStatus();
    }
}