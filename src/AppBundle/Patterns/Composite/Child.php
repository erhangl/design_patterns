<?php
/**
 * Oluşturulma tarihi : 30/03/2017
 */

namespace AppBundle\Patterns\Composite;

/**
 * Class Child
 *
 * @package AppBundle\Patterns\Composite
 */
final class Child extends AbstarctComponent
{
    public function run()
    {
        echo $this->name . '<br>';
    }
}