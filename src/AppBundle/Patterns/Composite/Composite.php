<?php
/**
 * Oluşturulma tarihi : 30/03/2017
 */

namespace AppBundle\Patterns\Composite;


use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Composite
 *
 * @package AppBundle\Patterns\Composite
 */
class Composite extends AbstarctComponent
{
    protected $componentList = [];

    /**
     * Composite constructor.
     *
     * @param $name
     */
    public function __construct($name)
    {
        $this->componentList = [];

        parent::__construct($name);
    }

    public function add(ComponentInterface $component)
    {
        if (!array_search($component, $this->componentList)) {
            $this->componentList[] = $component;
        }
    }

    public function remove(ComponentInterface $component)
    {
        if ($key = array_search($component, $this->componentList)) {
            unset($this->componentList[$key]);
        }
    }

    public function run()
    {
        echo $this->name . '<br>';
        foreach ($this->componentList as $component) {
            $component->run();
        }
    }
}