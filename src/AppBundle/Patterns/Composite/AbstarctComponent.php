<?php
/**
 * Oluşturulma tarihi : 30/03/2017
 */

namespace AppBundle\Patterns\Composite;

/**
 * Class AbstarctComponent
 *
 * @package AppBundle\Patterns\Composite
 */
abstract class AbstarctComponent implements ComponentInterface
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    abstract public function Run();
}