<?php
/**
 * Oluşturulma tarihi : 30/03/2017
 */

namespace AppBundle\Patterns\Composite;

interface ComponentInterface
{
    public function Run();
}