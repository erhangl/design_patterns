<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 05/07/2018
 * Time: 16:21
 */

namespace AppBundle\Patterns\Composite;


class PropertyComposite extends Property
{
    private $properties = [];

    public function add(Property $property) {
        $this->properties[] = $property;
    }

    public function getName()
    {
        $name = '';
        foreach ($this->properties as $property) {
            $name .= $property->getName();
        }

        return $name;
    }

    public function getValue()
    {
        $value = '';
        foreach ($this->properties as $property) {
            $value .= $property->getValue();
        }

        return $value;
    }
}