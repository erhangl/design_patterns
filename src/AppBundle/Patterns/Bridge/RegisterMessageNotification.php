<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

/**
 * Class RegisterMessageNotificationService
 *
 * @package AppBundle\Patterns\Bridge
 */
class RegisterMessageNotification extends AbstractNotification
{
    /**
     * @return string
     */
    public function getTemplate()
    {
        return 'Sayın %s; Sitemize hoşgeldiniz!';
    }

    /**
     * Sender send
     */
    public function send()
    {
        $this->getSender()->send($this->getTemplate());
    }
}