<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

/**
 * Class BlockMessageNotification
 *
 * @package AppBundle\Patterns\Bridge
 */
class BlockMessageNotification extends AbstractNotification
{
    /**
     * @return string
     */
    public function getTemplate()
    {
        return 'Sayın %s; Geçici olarak üyeliğiniz durdurulmuştur.';
    }

    public function send()
    {
        $this->getSender()->send($this->getTemplate());
    }
}