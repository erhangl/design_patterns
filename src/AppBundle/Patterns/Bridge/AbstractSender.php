<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

abstract class AbstractSender implements SenderInterface
{
    /** @var  User $user */
    protected $user;

    /**
     * AbstractSender constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user) {
        $this->user = $user;
    }

    abstract public function send($content);
}