<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

/**
 * Class SmsSender
 *
 * @package AppBundle\Patterns\Bridge
 */
class SmsSender extends AbstractSender
{
    /**
     * @param $content
     */
    public function send($content)
    {
        echo $this->user->getGsm() . ' numarasına '
             . sprintf($content, $this->getUser()->getName() . ' ' . $this->getUser()->getSurname())
             . ' içeriği sms olarak gönderildi<br>';
    }
}