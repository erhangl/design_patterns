<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

/**
 * Class AbstractBridge
 *
 * @package AppBundle\Patterns\Bridge
 */
abstract class AbstractNotification
{
    /** @var  SenderInterface $sender */
    protected $sender;

    /**
     * @return SenderInterface
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param SenderInterface $sender
     *
     * @return $this
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    abstract public function getTemplate();

    abstract public function send();
}