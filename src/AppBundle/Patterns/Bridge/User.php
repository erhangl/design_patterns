<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

/**
 * Class User
 *
 * @package AppBundle\Patterns\Bridge
 */
class User
{
    /** @var  string */
    private $name;

    /** @var  string */
    private $surname;

    /** @var  string */
    private $email;

    /** @var  string */
    private $gsm;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     *
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGsm()
    {
        return $this->gsm;
    }

    /**
     * @param mixed $gsm
     *
     * @return $this
     */
    public function setGsm($gsm)
    {
        $this->gsm = $gsm;

        return $this;
    }
}
