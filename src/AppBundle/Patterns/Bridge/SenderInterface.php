<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

/**
 * Interface SenderInterface
 *
 * @package AppBundle\Patterns\Bridge
 */
interface SenderInterface
{
    public function send($content);
}