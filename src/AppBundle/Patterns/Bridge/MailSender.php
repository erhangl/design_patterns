<?php
/**
 * Oluşturulma tarihi : 31/03/2017
 */

namespace AppBundle\Patterns\Bridge;

/**
 * Class MailSender
 *
 * @package AppBundle\Patterns\Bridge
 */
class MailSender extends AbstractSender
{
    /**
     * @param $content
     */
    public function send($content)
    {
        echo $this->user->getEmail() . ' email adresine '
             . sprintf($content, $this->getUser()->getName() . ' ' . $this->getUser()->getSurname())
             . ' içeriği email olarak gönderildi<br>';
    }
}
