<?php
/**
 * Oluşturulma tarihi : 25/04/2017
 */

namespace AppBundle\Patterns\Command;

/**
 * Class PushNotificationCommand
 *
 * @package AppBundle\Patterns\Command
 */
class PushNotificationCommand extends AbstractCommand
{
    /**
     * void
     */
    public function execute()
    {
        echo "Push notification gönderildi";
    }
}