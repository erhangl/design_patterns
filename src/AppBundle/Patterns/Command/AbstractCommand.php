<?php
/**
 * Oluşturulma tarihi : 24/04/2017
 */

namespace AppBundle\Patterns\Command;


use AppBundle\Patterns\Bridge\User;

/**
 * Class AbstractCommand
 *
 * @package AppBundle\Patterns\Command
 */
abstract class AbstractCommand implements CommandInterface
{
    /** @var User $user */
    protected $user;

    /**
     * AbstractCommand constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    abstract public function execute();
}
