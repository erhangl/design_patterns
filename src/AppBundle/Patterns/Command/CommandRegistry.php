<?php
/**
 * Oluşturulma tarihi : 24/04/2017
 */

namespace AppBundle\Patterns\Command;

/**
 * Class CommandRegistry
 *
 * @package AppBundle\Patterns\Command
 */
class CommandRegistry
{
    private $commands = [];

    /**
     * @param AbstractCommand $command
     *
     * @return $this
     */
    public function addCommand(AbstractCommand $command)
    {
        $this->commands[] = $command;

        return $this;
    }

    /**
     * @return void
     */
    public function execute()
    {
        foreach ($this->commands as $command) {
            $command->execute();
        }
    }

}