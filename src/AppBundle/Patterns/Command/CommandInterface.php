<?php

/**
 * Oluşturulma tarihi : 24/04/2017
 */

namespace AppBundle\Patterns\Command;

/**
 * Interface CommandInterface
 *
 * @package AppBundle\Patterns\Command
 */
interface CommandInterface
{
    public function execute();
}
