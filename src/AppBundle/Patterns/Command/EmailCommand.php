<?php
/**
 * Oluşturulma tarihi : 25/04/2017
 */

namespace AppBundle\Patterns\Command;

/**
 * Class EmailCommand
 *
 * @package AppBundle\Patterns\Command
 */
class EmailCommand extends AbstractCommand
{
    /**
     * void
     */
    public function execute()
    {
        echo "Email gönderildi<br>";
    }
}