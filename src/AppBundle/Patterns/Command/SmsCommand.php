<?php
/**
 * Oluşturulma tarihi : 25/04/2017
 */

namespace AppBundle\Patterns\Command;

/**
 * Class SmsCommand
 *
 * @package AppBundle\Patterns\Command
 */
class SmsCommand extends AbstractCommand
{
    /**
     * void
     */
    public function execute()
    {
        echo "Sms gönderildi<br>";
    }
}