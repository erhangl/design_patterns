<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 05/07/2018
 * Time: 16:42
 */

namespace AppBundle\Patterns\Strategy;


class CC extends PosStrategy
{
    public $method = "cc";

    public function pay()
    {
        echo "CC ok";
    }
}