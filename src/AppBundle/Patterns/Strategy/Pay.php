<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 05/07/2018
 * Time: 16:43
 */

namespace AppBundle\Patterns\Strategy;


class Pay
{
    public function pay(PosStrategy $pos, $name)
    {

        $pos->pay();
    }
}