<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 05/07/2018
 * Time: 16:44
 */

namespace AppBundle\Patterns\Strategy;


class PosPay
{
    private $method;

    private $strategies = [];

    /**
     * @param PosStrategy $strategy
     */
    public function add(PosStrategy $strategy)
    {
        $this->strategies[] = $strategy;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     *
     * @return PosPay
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    public function pay()
    {
        /** @var PosStrategy $strategy */
        foreach ($this->strategies as $strategy) {
            if ($strategy->method == $this->method) {
                $strategy->pay();
            }
        }
    }
}