<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 05/07/2018
 * Time: 16:41
 */

namespace AppBundle\Patterns\Strategy;


class Wallet extends PosStrategy
{
    public $method = 'wallet';

    public function pay()
    {
        echo "wallet ok <br>";
    }
}