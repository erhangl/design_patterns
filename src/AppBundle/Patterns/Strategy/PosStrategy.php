<?php
/**
 * Created by IntelliJ IDEA.
 * User: erhangul
 * Date: 05/07/2018
 * Time: 16:40
 */

namespace AppBundle\Patterns\Strategy;


abstract class PosStrategy
{
    abstract public function pay();
}